;; == cpp_completion.el ==
;; Author: Gregory J. Stein
;; Email: gregory.j.stein@gmail.com
;; License: None; feel free to use at will.
(setq inhibit-startup-screen t)
;; == Set up use-package ==
;; Uncomment the following if use-package is not installed
(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/"))
(add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/"))
(package-initialize)
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(eval-when-compile (require 'use-package))

;; == irony-mode ==
(use-package irony
  :ensure t
  :defer t
  :init
  (add-hook 'c++-mode-hook 'irony-mode)
  (add-hook 'c-mode-hook 'irony-mode)
  (add-hook 'objc-mode-hook 'irony-mode)
  :config
  ;; replace the `completion-at-point' and `complete-symbol' bindings in
  ;; irony-mode's buffers by irony-mode's function
  (defun my-irony-mode-hook ()
    (define-key irony-mode-map [remap completion-at-point]
      'irony-completion-at-point-async)
    (define-key irony-mode-map [remap complete-symbol]
      'irony-completion-at-point-async))
  (add-hook 'irony-mode-hook 'my-irony-mode-hook)
  (add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)
  )

;; == company-mode ==
(use-package company
  :ensure t
  :defer t
  :init (add-hook 'after-init-hook 'global-company-mode)
  :config
  (use-package company-irony :ensure t :defer t)
  (setq company-idle-delay              nil
	company-minimum-prefix-length   2
	company-show-numbers            t
	company-tooltip-limit           20
	company-dabbrev-downcase        nil
	company-backends                '((company-irony company-gtags))
	)
  :bind ("C-;" . company-complete-common)
)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (tango-dark)))
 '(package-selected-packages
   (quote
    (magic-latex-buffer latex-preview-pane latex-pretty-symbols latex-math-preview latex-extra company-math ac-math doctags cmake-ide cpputils-cmake cmake-mode magit flx-ido python-pep8 pep8 flymake-python-pyflakes flycheck-pyflakes company-quickhelp sphinx-doc python pyflakes anaconda-mode elpy pyde jedi color-theme-cobalt color-theme lush-theme inkpot-theme helm-projectile ipython company-c-headers company-jedi company-irony-c-headers company-irony company irony use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;;; Extra C++
(with-eval-after-load 'company
  (require 'flycheck)
  (require 'company-quickhelp)
  (add-hook 'c++-mode-hook 'flycheck-mode)
  (add-hook 'c++-mode-hook 'company-quickhelp-mode))



;;; C++ file extensions
(add-to-list 'auto-mode-alist '("\\.cc\\'" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.hh\\'" . c++-mode))

;;; Python company

(with-eval-after-load 'company
  (require 'company-jedi)
  (autoload 'jedi:setup "jedi" nil t)
  (add-hook 'python-mode-hook 'jedi:setup)
  (add-hook 'python-mode-hook 'company-quickhelp-mode)
  (add-to-list 'company-backends 'company-jedi)
  )

(with-eval-after-load 'anaconda
  (require 'anaconda-mode)
  (add-hook 'python-mode-hook 'anaconda-mode)
  (add-hook 'python-mode-hook 'anaconda-eldoc-mode))

(with-eval-after-load 'flycheck
  (require 'flycheck-pyflakes)
  (add-hook 'python-mode-hook 'flycheck-mode))

;;; Python keybindings

(with-eval-after-load 'python-nav
  (define-key python-mode-map (kbd "M-p") 'python-nav-backward-defun)
  (define-key python-mode-map (kbd "M-n") 'python-nav-forward-defun))


;;; Guimacs customization
(menu-bar-mode -1)
(tool-bar-mode -1)
(toggle-scroll-bar -1)

;;; Projectile global
(projectile-global-mode)
(put 'erase-buffer 'disabled nil)
